import "reflect-metadata";
import { InversifyExpressServer } from "inversify-express-utils";
import { ContainerFactory } from "./configs/container/container-factory";
import { ExpressConfigFactory } from "./configs/express.config.factory";
import { ErrorHandlerFactory } from "./configs/error.handler.config.factory";
import { logger } from "./configs/logger";
import { properties } from "./configs/properties";

const server = new InversifyExpressServer(ContainerFactory.create(), null, { rootPath: properties.baseUrl.v1 });
server.setConfig(ExpressConfigFactory.create());
server.setErrorConfig(ErrorHandlerFactory.create());

const app: any = server.build();
app.listen(properties.port, function () {
    logger.info("Server is listening on port", properties.port);
});

import { DatabaseGateway } from "@gateways/database/database.gateway";
import { GATEWAY_TYPES } from "../configs/container/gateway-types";

import { injectable, inject } from "inversify";
import { User } from "src/domain/user";

import { logger } from "@configs/logger";

@injectable()
export class SaveUserUseCase {
    public static TARGET_NAME = "SaveUserUseCase";
    constructor(@inject(GATEWAY_TYPES.databaseGateway) private databaseGateway: DatabaseGateway) {}

    public save(user: User): Promise<string> {
        logger.info("Start", user);

        const userIdPromisse = this.databaseGateway.save(user);

        logger.info("end", userIdPromisse);
        return userIdPromisse;
    }
}

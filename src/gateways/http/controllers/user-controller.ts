import { SaveUserUseCase } from "@usecases/save-user.usecase";
import { Response, Request, NextFunction } from "express";
import { inject } from "inversify";
import { controller, httpPost } from "inversify-express-utils";

import { logger } from "@configs/logger";
import { UserJson } from "./json/user.json";
import { User } from "src/domain/user";

@controller("/users")
export class UserController {
    public static TARGET_NAME = "RegistrationController";
    constructor(@inject(SaveUserUseCase.TARGET_NAME) private saveUserUseCase: SaveUserUseCase) {}

    @httpPost("/")
    public create(request: Request, response: Response, next: NextFunction): void {
        logger.info("Start");

        const userJsonToCreate: UserJson = request.body;
        logger.info("Request Body", userJsonToCreate);

        const userToCreate = this.mapperUserFromUserJson(userJsonToCreate);

        this.saveUserUseCase
            .save(userToCreate)
            .then((userId) => {
                response.statusCode = 201;
                response.send({
                    userId: userId,
                });

                logger.info("end");
            })
            .catch((error) => {
                logger.error(error.message, error);
                next(error);
            });
    }

    private mapperUserFromUserJson(userJson: UserJson): User {
        return new User(userJson.id, userJson.name, userJson.email, userJson.password);
    }
}

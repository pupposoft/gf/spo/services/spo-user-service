export interface BaseException {
    code: string;
    message: string;
}

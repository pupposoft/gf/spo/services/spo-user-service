import { Container } from "inversify";
import { interfaces, TYPE } from "inversify-express-utils";
import { UserController } from "../../gateways/http/controllers/user-controller";
import { SaveUserUseCase } from "../../use-cases/save-user.usecase";
import { DatabaseGateway } from "../../gateways/database/database.gateway";
import { MongoDataBaseGateway } from "../../gateways/database/mongo/mongo-database.gateway";
import { GATEWAY_TYPES } from "../../configs/container/gateway-types";

export class ContainerFactory {
    public static create(): Container {
        const container: Container = new Container();

        container
            .bind<interfaces.Controller>(TYPE.Controller)
            .to(UserController)
            .inSingletonScope()
            .whenTargetNamed(UserController.TARGET_NAME);

        container.bind<SaveUserUseCase>(SaveUserUseCase.TARGET_NAME).to(SaveUserUseCase).inSingletonScope();

        container.bind<DatabaseGateway>(GATEWAY_TYPES.databaseGateway).to(MongoDataBaseGateway).inSingletonScope();

        return container;
    }
}

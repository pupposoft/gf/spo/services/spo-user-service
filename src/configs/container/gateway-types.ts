const LABEL_TYPES = {
    databaseGateway: "mongoDataBaseGateway",
};

export const GATEWAY_TYPES = {
    databaseGateway: Symbol.for(LABEL_TYPES.databaseGateway),
};

import { interfaces } from "inversify-express-utils";
import * as bodyParser from "body-parser";

export class ExpressConfigFactory {
    public static create(): interfaces.ConfigFunction {
        return (app: any) => {
            app.use(bodyParser.json());
        };
    }
}

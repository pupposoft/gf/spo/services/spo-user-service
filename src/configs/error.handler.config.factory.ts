import * as express from "express";
import { interfaces } from "inversify-express-utils";
//import { BusinessException } from '../../domains/business.exception';
//import { ApiClientException } from '../gateways/exception/api.client.exception';

export class ErrorHandlerFactory {
    public static create(): interfaces.ConfigFunction {
        return (app: any) => {
            app.use((
                /*err: Error, request: express.Request,*/ response: express.Response /*next: express.NextFunction*/
            ) => {
                //TODO: Implementar
                response
                    .status(500)
                    .json([
                        {
                            code: "INTERNAL_SERVER_ERROR",
                            message: "Internal Server Error.",
                        },
                    ])
                    .end();

                // if (err instanceof BusinessException) {
                //   let be: BusinessException = (err as BusinessException)
                //   response.status(be.httpStatus).json([{
                //     code: be.code,
                //     message: be.message,
                //   }]).end();
                // } else if (err instanceof ApiClientException) {
                //
                //   this.handleApiClientException(err, response);
                //
                // } else {
                //   response.status(500).json([{
                //     code: 'INTERNAL_SERVER_ERROR',
                //     message: 'Internal Server Error.',
                //   }]).end();
                // }
            });
        };
    }

    // private static handleApiClientException(err: Error, response: express.Response): void {
    //   let ace: ApiClientException = (err as ApiClientException)
    //
    //   if (ace.httpStatus === 400) {
    //
    //     response.status(ace.httpStatus).json(JSON.parse(ace.body)).end();
    //
    //   } else if (ace.httpStatus === 401) {
    //     response.status(ace.httpStatus).end();
    //   }else if (ace.httpStatus === 500) {
    //
    //     response.status(422).json([{
    //       code: this.appName() + '.unavailability',
    //       message: 'Resource is unavailability. Try again.',
    //     }]).end();
    //
    //   } else {
    //
    //     let jsonData: JsonException[] = [];
    //     let appName: string = this.appName();
    //     JSON.parse(ace.body).forEach((error) => {
    //       jsonData.push(new JsonException(appName + '.' + error.code.split('.')[1], error.message));
    //     });
    //
    //     response.status(ace.httpStatus).json(jsonData).end();
    //   }
    // }
    //
    // private static appName() : string {
    //   return config.app.replace('vsc-bff-', '');
    // }
}

export class JsonException {
    public code: string;
    public message: string;

    constructor(code: string, message: string) {
        this.code = code;
        this.message = message;
    }
}

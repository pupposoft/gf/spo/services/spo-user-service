import { createLogger, format, transports } from "winston";

export const logger = createLogger({
    format: format.combine(format.timestamp(), format.json()),
    defaultMeta: { service: "user-service" },
    transports: [
        new transports.File({
            maxsize: 20000000,
            filename: "logs/error.log",
            level: "error",
        }),
        new transports.File({
            maxsize: 20000000,
            filename: "logs/info.log",
            level: "info",
        }),
    ],
});

if (process.env.NODE_ENV !== "production") {
    logger.add(
        new transports.Console({
            format: format.simple(),
        })
    );
}

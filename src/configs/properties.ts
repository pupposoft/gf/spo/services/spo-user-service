// eslint-disable-next-line @typescript-eslint/no-var-requires
require("custom-env").env(true, "environments/");

export const properties = {
    env: process.env.APP_ENV,
    appName: "spo-user-service",
    baseUrl: {
        v1: "/spo/v1",
    },
    port: 8000,
};

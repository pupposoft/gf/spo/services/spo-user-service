module.exports = {
    env: {
        es6: true,
        node: true,
        jest: true,
    },
    parser: "@typescript-eslint/parser",
    extends: [
        "plugin:@typescript-eslint/recommended",
        "prettier/@typescript-eslint",
        "plugin:prettier/recommended",
    ],
    globals: {
        Atomics: "readonly",
        SharedArrayBuffer: "readonly",
    },
    parserOptions: {
        ecmaVersion: 11,
        sourceType: "module",
    },
    plugins: ["@typescript-eslint", "prettier", "eslint-plugin-import-helpers"],
    rules: {
        "max-len": [
            2,
            {
                code: 120,
                tabWidth: 4,
                ignoreUrls: true,
                ignoreStrings: true,
                ignoreTemplateLiterals: true,
                ignoreRegExpLiterals: true,
                ignoreComments: false,
            },
        ],
        "no-new": "off",
        "no-prototype-builtins": "off",
        "no-restricted-syntax": "off",
        "max-classes-per-file": "off",
        "@typescript-eslint/no-explicit-any": "off",
        "no-console": "off",
        "import/prefer-default-export": "off",
        "@typescript-eslint/explicit-function-return-type": ["off"],
        "@typescript-eslint/ban-types": [
            "warn",
            {
                types: {
                    object: false,
                },
            },
        ],
        "@typescript-eslint/no-unused-vars": [
            "error",
            {
                argsIgnorePattern: "_",
            },
        ],
        "no-useless-constructor": "off",
        "@typescript-eslint/naming-convention": [
            "error",
            {
                selector: "interface",
                format: ["PascalCase"],
                custom: {
                    regex: "^[A-Z]",
                    match: true,
                },
            },
        ],
        "@typescript-eslint/explicit-module-boundary-types": [
            "warn",
            {
                allowArgumentsExplicitlyTypedAsAny: true,
            },
        ],
        "no-underscore-dangle": "off",
        "@typescript-eslint/camelcase": "off",
        "prettier/prettier": "error",
        "class-methods-use-this": "off",
        "import-helpers/order-imports": [
            "warn",
            {
                newlinesBetween: "always", // new line between groups
                groups: [
                    "module",
                    "/@gateways/",
                    "/@usecases/",
                    "/@domains/",
                    "/@configs/",
                    "/@exceptions/",
                    ["parent", "sibling", "index"],
                ],
                alphabetize: { order: "asc", ignoreCase: true },
            },
        ],
    },
};

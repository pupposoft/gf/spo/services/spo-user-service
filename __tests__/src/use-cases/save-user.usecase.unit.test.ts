import "reflect-metadata";
import { mock } from "jest-mock-extended";
import { userFull } from "../../__fixture__/domains/user.template";
import { DatabaseGateway } from "../../../src/gateways/database/database.gateway";
import { SaveUserUseCase } from "../../../src/use-cases/save-user.usecase";

describe("Unit Tests of SaveUserUseCase", () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    const expectedUserId = "anyUserId";
    const userToSave = userFull.build();

    it("Save user with success", async () => {
        const mockedUserDatabaseGateway = mock<DatabaseGateway>();
        mockedUserDatabaseGateway.save.calledWith(userToSave).mockResolvedValue(expectedUserId);

        const saveUserUseCase = new SaveUserUseCase(mockedUserDatabaseGateway);

        const newUserIdPromise = saveUserUseCase.save(userToSave);

        newUserIdPromise.then((newUserId: string) => {
            expect(newUserId).toEqual(expectedUserId);
        });

        const userCaptured = mockedUserDatabaseGateway.save.mock.calls[0][0];
        expect(userToSave.id).toEqual(userCaptured.id);
        expect(userToSave.name).toEqual(userCaptured.name);
        expect(userToSave.email).toEqual(userCaptured.email);
        expect(userToSave.password).toEqual(userCaptured.password);
    });
});

import * as factory from "factory.ts";
import faker = require("faker");
//import faker from "faker";

import { User } from "../../../src/domain/user";

export const userFull = factory.Sync.makeFactory<User>({
    id: faker.random.alpha(),
    email: faker.random.alpha(),
    name: faker.random.alpha(),
    password: faker.random.alpha(),
});
